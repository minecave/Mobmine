package com.minecave.mobmine;

import java.util.HashMap;
import java.util.Map;

import lombok.Getter;

import org.bukkit.configuration.ConfigurationSection;

import com.google.common.collect.Maps;

public class Mine {
	@Getter
	private HashMap<String, MobSpawnData> mobs;

	public Mine(ConfigurationSection sec, String world) {
		this.mobs = Maps.newHashMap();
		sec = sec.getConfigurationSection("mobs");
		for (String i : sec.getKeys(false)) {
			try {
				this.mobs.put(i, new MobSpawnData(i, sec.getConfigurationSection(i), world));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void tick() {
		for (Map.Entry<String, MobSpawnData> i : this.mobs.entrySet()) {
			i.getValue().spawn(MinecaveMobMines.getPlugin().getMines().get(this));
		}
	}
}
