package com.minecave.mobmine;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.stream.Collectors;

import lombok.Getter;
import net.minecraft.server.v1_8_R3.BiomeBase;
import net.minecraft.server.v1_8_R3.EntityCaveSpider;
import net.minecraft.server.v1_8_R3.EntityInsentient;
import net.minecraft.server.v1_8_R3.EntityIronGolem;
import net.minecraft.server.v1_8_R3.EntitySpider;
import net.minecraft.server.v1_8_R3.EntityTypes;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockDispenseEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.SlimeSplitEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.minecave.mobmine.util.AggroCaveSpider;
import com.minecave.mobmine.util.AggroIronGolem;
import com.minecave.mobmine.util.AggroSpider;
import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class MinecaveMobMines extends JavaPlugin implements Listener {
	@Getter
    private static MinecaveMobMines plugin;
    
	@Getter
    private HashMap<Mine, ProtectedRegion> mines;
	
	@Getter
	private boolean debug;
	
	private World mobMineWorld, spawnWorld;

    public void addEnchants() {
        if (Enchantment.getById(129) != null) {
            return;
        }
        try {
            Field f = Enchantment.class.getDeclaredField("acceptingNew");
            f.setAccessible(true);
            f.set(null, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        EnchantGlow glow = new EnchantGlow(129);
        Enchantment.registerEnchantment(glow);
    }

    public void registerEntity(String name, int id, Class<? extends EntityInsentient> nmsClass, Class<? extends EntityInsentient> customClass) {
        try {
            List<Map<?, ?>> dataMaps = Lists.newArrayList();
            for (Field f : EntityTypes.class.getDeclaredFields()) {
                if (f.getType().getSimpleName().equals(Map.class.getSimpleName())) {
                    f.setAccessible(true);
                    dataMaps.add((Map) f.get(null));
                }
            }
            if (((Map) dataMaps.get(2)).containsKey(id)) {
                ((Map) dataMaps.get(0)).remove(name);
                ((Map) dataMaps.get(2)).remove(id);
            }
            Method method = EntityTypes.class.getDeclaredMethod("a", Class.class, String.class, Integer.TYPE);
            method.setAccessible(true);
            method.invoke(null, customClass, name, id);
            for (Field f : BiomeBase.class.getDeclaredFields()) {
                if ((f.getType().getSimpleName().equals(BiomeBase.class.getSimpleName())) &&
                        (f.get(null) != null)) {
                    for (Field list : BiomeBase.class.getDeclaredFields()) {
                        if (list.getType().getSimpleName().equals(List.class.getSimpleName())) {
                            list.setAccessible(true);

                            List<BiomeBase.BiomeMeta> metaList = (List) list.get(f.get(null));
                            for (BiomeBase.BiomeMeta meta : metaList) {
                                Field clazz = BiomeBase.BiomeMeta.class.getDeclaredFields()[0];
                                if (clazz.get(meta).equals(nmsClass)) {
                                    clazz.set(meta, customClass);
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @EventHandler
    public void armor(EntityDamageByEntityEvent event) {
        if (event.getEntity().getType() != EntityType.PLAYER) {
            return;
        }
        if (!event.getEntity().getLocation().getWorld().getName().equals("mob-mine")) {
            return;
        }
        Player p = (Player) event.getEntity();
        try {
            if ((p.getEquipment().getHelmet() != null) && (p.getEquipment().getHelmet().getItemMeta() != null)) {
                p.getEquipment().getHelmet().setDurability((short) (p.getEquipment().getHelmet().getDurability() - 10));
            }
            if ((p.getEquipment().getChestplate() != null) && (p.getEquipment().getChestplate().getItemMeta() != null)) {
                p.getEquipment().getChestplate().setDurability((short) (p.getEquipment().getChestplate().getDurability() - 10));
            }
            if ((p.getEquipment().getLeggings() != null) && (p.getEquipment().getLeggings().getItemMeta() != null)) {
                p.getEquipment().getLeggings().setDurability((short) (p.getEquipment().getLeggings().getDurability() - 10));
            }
            if ((p.getEquipment().getBoots() != null) && (p.getEquipment().getBoots().getItemMeta() != null)) {
                p.getEquipment().getBoots().setDurability((short) (p.getEquipment().getBoots().getDurability() - 10));
            }
        } catch (Exception ignored) {
        }
    }

    @Override
    public void onEnable() {
        plugin = this;
        mobMineWorld = Bukkit.getWorld(getConfig().getString("world-definitions.mob-mine"));
        spawnWorld = Bukkit.getWorld(getConfig().getString("world-definitions.spawn"));

        addEnchants();
        registerEntity("AggroSpider", 52, EntitySpider.class, AggroSpider.class);
        registerEntity("AggroCaveSpider", 59, EntityCaveSpider.class, AggroCaveSpider.class);
        registerEntity("AggroGolem", 99, EntityIronGolem.class, AggroIronGolem.class);

        new BukkitRunnable() {
            public void run() {
                saveDefaultConfig();
                getServer().getPluginManager().registerEvents(MinecaveMobMines.getPlugin(), MinecaveMobMines.getPlugin());
                loadconf();


                new BukkitRunnable() {
                    public void run() {
                        mobMineWorld.getLivingEntities().stream()
                                .filter(i -> (i instanceof Monster))
                                .filter(i -> MinecaveMobMines.this.getRegionIsIn(i.getLocation()) == null).collect(Collectors.toList())
                                .forEach(entity -> entity.setHealth(0));
                        spawnWorld.getLivingEntities().stream()
                                .filter(i -> (i instanceof Monster))
                                .filter(i -> MinecaveMobMines.this.getRegionIsIn(i.getLocation()) == null).collect(Collectors.toList())
                                .forEach(entity -> entity.setHealth(0));
                    }
                }.runTaskTimer(MinecaveMobMines.getPlugin(), 5L, 10L);


                new BukkitRunnable() {
                    public void run() {
                        for (Map.Entry<Mine, ProtectedRegion> i : getMines().entrySet()) {
                            try {
                                i.getKey().tick();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }.runTaskTimer(MinecaveMobMines.getPlugin(), 20L, 20L);
            }
        }.runTaskLater(this, 1L);
    }

    @EventHandler
    public void interact(PlayerInteractEvent event) {
        if ((event.getItem() != null) && ((event.getItem().getType() == Material.MONSTER_EGG) || (event.getItem().getType() == Material.MONSTER_EGGS)) &&
                (event.getItem().hasItemMeta()) &&
                (event.getItem().getItemMeta().getLore() != null)) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void playerDeath(PlayerDeathEvent event) {
        if (event.getEntity().getKiller() == null) {
            event.setDeathMessage("");
        }
        
        ProtectedRegion region = getRegionIsIn(event.getEntity().getLocation());
        if (region == null)
        	return;
        
        if (getKeyByValue(mines, region) != null)
        	event.setKeepInventory(true);
    }

    @EventHandler
    public void silverfishEnter(EntityChangeBlockEvent e) {
        if (e.getTo() == Material.MONSTER_EGGS) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void slimeSplit(SlimeSplitEvent event) {
    	if (!event.getEntity().getWorld().getUID().equals(mobMineWorld.getUID()) && ! event.getEntity().getWorld().getUID().equals(spawnWorld.getUID()))
    		return;
    	
        event.setCount(0);
    }

    @EventHandler
    public void creatureSpawn(CreatureSpawnEvent event) {
    	if (!event.getLocation().getWorld().getUID().equals(mobMineWorld.getUID()) && ! event.getLocation().getWorld().getUID().equals(spawnWorld.getUID()))
    		return;
    	
        if ((event.getEntity().getType() == EntityType.SLIME) && (event.getSpawnReason() == CreatureSpawnEvent.SpawnReason.SLIME_SPLIT)) {
            event.setCancelled(true);
        }
        if (event.getSpawnReason() == CreatureSpawnEvent.SpawnReason.BUILD_WITHER) {
            event.setCancelled(true);
        }
        
        int id = event.getEntity().getType().getTypeId();
        if ((id == 52 || id == 59 || id == 99) && event.getSpawnReason() != CreatureSpawnEvent.SpawnReason.CUSTOM) {
        	event.setCancelled(true);
        }
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (cmd.getName().equalsIgnoreCase("sgnedit") ) {
            Player p = (Player) sender;
            Block b = p.getTargetBlock((Set) null, 10);
            Sign s = (Sign) b.getState();
            String c = "";
            for (int a = 1; a < args.length; a++) {
                c = c + args[a] + " ";
            }
            c.substring(0, c.length() - 1);
            String d = ChatColor.translateAlternateColorCodes('&', c);
            s.setLine(Integer.parseInt(args[0]), d);
            s.update();
            return true;
        }
        if (cmd.getName().equalsIgnoreCase("mobminedebug")) {
        	if (!sender.hasPermission(cmd.getPermission())) {
        		sender.sendMessage(ChatColor.translateAlternateColorCodes('&', cmd.getPermissionMessage()));
        		return true;
        	}
        	debug = !debug;
        	sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "Mobmine debug mode set to: &6" + debug));
        }
        return true;
    }

    @EventHandler
    public void drop(BlockDispenseEvent e) {
        if (e.getItem().getType() == Material.MONSTER_EGG) {
            e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void mobDeath(EntityDeathEvent e) {
        if(e.getEntity() == null){
            return;
        }
        if(e.getEntity().hasMetadata("Mobmine")){
            String nam = e.getEntity().getCustomName();
            if (nam != null) {
                ProtectedRegion d = getRegionIsIn(e.getEntity().getLocation());
                if (d != null) {
                    Mine mine = getKeyByValue(mines, d);
                    e.getDrops().clear();
                    if (mine != null){
                        if (mine.getMobs().containsKey(nam)) {
                            e.getDrops().addAll(mine.getMobs().get(nam).getDrops());
                        }
                    }
                }
            }
        }
    }

    public void debug(String message) {
    	if (debug)
    		getLogger().log(Level.WARNING, message);
    }
    public static <T, E> T getKeyByValue(Map<T, E> map, E value) {
        for (Map.Entry<T, E> entry : map.entrySet()) {
            if (value.equals(entry.getValue())) {
                return entry.getKey();
            }
        }
        return null;
    }

    public ProtectedRegion getRegionIsIn(Location l) {
        RegionManager man = WGBukkit.getPlugin().getRegionManager(l.getWorld());
        ApplicableRegionSet set = man.getApplicableRegions(l);
        for (ProtectedRegion reg : set) {
            if (mines.containsValue(reg)) {
                return reg;
            }
        }
        return null;
    }

    public void loadconf() {
        debug = getConfig().contains("debug") ? getConfig().getBoolean("debug") : false;
        mines = Maps.newHashMap();
        ConfigurationSection conf = getConfig().getConfigurationSection("worlds");
        for (String world : conf.getKeys(false)) {
            ConfigurationSection worldSec = conf.getConfigurationSection(world);
            for (String i : worldSec.getKeys(false)) {
                mines.put(new Mine(worldSec.getConfigurationSection(i), world), WGBukkit.getPlugin().getRegionManager(Bukkit.getWorld(world)).getRegion(i));
            }
        }
    }

    public void saveconf() {
        saveConfig();
    }
}
