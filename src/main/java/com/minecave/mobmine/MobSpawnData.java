package com.minecave.mobmine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockFace;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.minecave.mobmine.util.AggroCaveSpider;
import com.minecave.mobmine.util.AggroIronGolem;
import com.minecave.mobmine.util.AggroSpider;
import com.minecave.mobmine.util.ItemUtilzzz;
import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class MobSpawnData {
	private mobInjector injector;
	private String spawnFunction;
	private String maxmob;
	private EntityType type;
	private int altdat;
	private String name;
	private String WRLD;
	private HashMap<Double, List<ItemStack>> drops;

	public MobSpawnData(String n, ConfigurationSection sec, String world) {
		this.drops = Maps.newHashMap();
		this.WRLD = world;
		this.name = n;
		this.maxmob = sec.getString("max");
		double tmp = -1.0D;
		try {
			tmp = Double.parseDouble(sec.getString("type"));
		} catch (Exception ignored) {
		}
		if (tmp == -1.0D) {
			this.type = EntityType.valueOf(sec.getString("type"));
		} else {
			this.type = EntityType.fromId((int) tmp);
		}
		if (this.type == null) {
			this.altdat = ((int) tmp);
		}
		this.spawnFunction = sec.getString("spawnFunction");
		this.injector = new mobInjector(n, sec.getConfigurationSection("data"));
		ConfigurationSection dropsec = sec.getConfigurationSection("drops");
		for (String i : dropsec.getKeys(false)) {
			this.drops.put(dropsec.getDouble(i), ItemUtilzzz.itemFromString(i));
		}
	}

	public void spawn(ProtectedRegion reg) {
		int players = 0;
		List<LivingEntity> ents = Lists.newArrayList();
		for (LivingEntity i : Bukkit.getServer().getWorld(this.WRLD).getLivingEntities()) {
			if (inRegion(reg, i.getLocation(), 50.0D)) {
				if (i.getType() == EntityType.PLAYER) {
					players++;
				} else if (this.name.equals(i.getCustomName())) {
					ents.add(i);
				} else if (i.getCustomName() == null) {
					i.remove();
				}
			}
		}
		if (players > 0) {
			Expression maxmobs = new ExpressionBuilder(this.maxmob).variable("X").build().setVariable("X", players);

			int diff = (int) maxmobs.evaluate() - ents.size();
			Expression e = new ExpressionBuilder(this.spawnFunction).variable("X").build().setVariable("X", players);
			int needtospawn = (int) e.evaluate();
			if (diff <= 0) {
				return;
			}
			while ((diff > 0) && (needtospawn > 0)) {
				Location l = getRandom(reg);
				int cnt = 50;
				while ((l.getBlock().getType() != Material.AIR)
						|| (l.getBlock().getRelative(BlockFace.DOWN).getType() == Material.AIR)
						|| (l.getBlock().getRelative(BlockFace.EAST).getType() != Material.AIR)
						|| (l.getBlock().getRelative(BlockFace.NORTH).getType() != Material.AIR)
						|| (l.getBlock().getRelative(BlockFace.WEST).getType() != Material.AIR)
						|| (l.getBlock().getRelative(BlockFace.SOUTH).getType() != Material.AIR)) {
					cnt--;
					if (cnt < 0) {
						break;
					}
					l = getRandom(reg);
				}
				l = l.add(0.5D, 0.0D, 0.5D);

				LivingEntity i = null;
				if (this.type != null) {
					i = (LivingEntity) Bukkit.getWorld(this.WRLD).spawnEntity(l, this.type);
				} else if (this.altdat == 69) {
					MinecaveMobMines.getPlugin().debug("spawning aggro... diff: " + diff + "; ents: " + ents.size() + "; players: " + players);
					AggroSpider spi = new AggroSpider(((CraftWorld) l.getWorld()).getHandle());
					spi.setPosition(l.getX(), l.getY(), l.getZ());
					((CraftWorld) l.getWorld()).getHandle().addEntity(spi, CreatureSpawnEvent.SpawnReason.CUSTOM);
					i = (LivingEntity) spi.getBukkitEntity();
				} else if (this.altdat == 70) {
					AggroCaveSpider spi = new AggroCaveSpider(((CraftWorld) l.getWorld()).getHandle());
					spi.setPosition(l.getX(), l.getY(), l.getZ());
					((CraftWorld) l.getWorld()).getHandle().addEntity(spi, CreatureSpawnEvent.SpawnReason.CUSTOM);
					i = (LivingEntity) spi.getBukkitEntity();
				} else if (this.altdat == 71) {
					AggroIronGolem spi = new AggroIronGolem(((CraftWorld) l.getWorld()).getHandle());
					spi.setPosition(l.getX(), l.getY(), l.getZ());
					((CraftWorld) l.getWorld()).getHandle().addEntity(spi, CreatureSpawnEvent.SpawnReason.CUSTOM);
					i = (LivingEntity) spi.getBukkitEntity();
				}
				diff--;
				needtospawn--;
				if (i != null) {
					this.injector.apply(i);
				}
			}
		} else {
			ents.forEach(org.bukkit.entity.LivingEntity::remove);
		}
		ents.clear();
	}

	public Location getRandom(ProtectedRegion reg) {
		BlockVector max = reg.getMaximumPoint();
		BlockVector min = reg.getMinimumPoint();
		Vector res = min.add(random.nextInt(max.getBlockX() - min.getBlockX()),
				random.nextInt(max.getBlockY() - min.getBlockY()),
				random.nextInt(max.getBlockZ() - min.getBlockZ()));
		return new Location(Bukkit.getWorld(this.WRLD), res.getX(), res.getY(), res.getZ());
	}

	public boolean inRegion(ProtectedRegion reg, Location l, double tolerance) {
		return reg.contains(l.getBlockX(), l.getBlockY(), l.getBlockZ());
	}

	static Random random = new Random();

	public List<ItemStack> getDrops() {
		ArrayList<ItemStack> ret = Lists.newArrayList();
		this.drops
				.entrySet()
				.stream()
				.filter(ent -> random.nextInt(100) > ent.getKey())
				.forEach(
						ent -> ret.addAll(ent.getValue().stream()
								.map(ItemStack::new)
								.collect(Collectors.toList())));
		return ret;
	}
}
