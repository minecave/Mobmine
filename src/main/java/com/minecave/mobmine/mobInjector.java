package com.minecave.mobmine;

import com.minecave.mobmine.util.ItemUtilzzz;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.*;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.PotionEffect;

import java.util.ArrayList;

class mobInjector
{
   String mobname;
   ArrayList<PotionEffect> pe;
   ItemStack[] equips;
   boolean baby;
   boolean wither;
   ItemStack weapon;
   double hp = 20.0D;
   int size = 4;

   public mobInjector(String n, ConfigurationSection sec)
   {
      this.pe = new ArrayList();
      this.mobname = n;
      for (String i : sec.getConfigurationSection("potionEffects").getKeys(false)) {
         this.pe.add(new PotionEffect(sec.getConfigurationSection("potionEffects." + i).getValues(false)));
      }
      this.baby = sec.getBoolean("baby");
      this.wither = sec.getBoolean("wither");
      this.hp = sec.getDouble("hp");
      this.size = sec.getInt("size");
      sec = sec.getConfigurationSection("equips");
      this.equips = new ItemStack[4];
      this.equips[3] = ((ItemStack) ItemUtilzzz.itemFromString(sec.getString("helmet")).get(0));
      this.equips[2] = ((ItemStack)ItemUtilzzz.itemFromString(sec.getString("chestplate")).get(0));
      this.equips[1] = ((ItemStack)ItemUtilzzz.itemFromString(sec.getString("leggings")).get(0));
      this.equips[0] = ((ItemStack)ItemUtilzzz.itemFromString(sec.getString("boots")).get(0));

      this.weapon = ((ItemStack)ItemUtilzzz.itemFromString(sec.getString("weapon")).get(0));
   }

   public LivingEntity apply(LivingEntity in)
   {
      if (in.getType() == EntityType.ZOMBIE)
      {
         Zombie zom = (Zombie)in;
         zom.setBaby(this.baby);
      }
      else if (in.getType() == EntityType.PIG_ZOMBIE)
      {
         PigZombie pzom = (PigZombie)in;
         pzom.setBaby(this.baby);
      }
      else if (in.getType() == EntityType.WOLF)
      {
         Wolf w = (Wolf)in;
         w.setAngry(true);
      }
      else if ((in.getType() == EntityType.MAGMA_CUBE) || (in.getType() == EntityType.SLIME))
      {
         Slime s = (Slime)in;
         s.setSize(this.size);
      }
      if ((in.getType() == EntityType.SKELETON) && (this.wither))
      {
         Skeleton sk = (Skeleton)in;
         sk.setSkeletonType(Skeleton.SkeletonType.WITHER);
      }
      if (in.getPassenger() != null) {
         in.getPassenger().remove();
      }
      in.addPotionEffects(this.pe);
      in.setCustomNameVisible(true);
      in.setCanPickupItems(false);
      in.setCustomName(this.mobname);
      in.setMetadata("Mobmine", new FixedMetadataValue(MinecaveMobMines.getPlugin(), true));
      EntityEquipment equi = in.getEquipment();
      if (equi == null) {
         return in;
      }
      equi.setItemInHand(this.weapon);
      equi.setItemInHandDropChance(0.0F);
      equi.setArmorContents(this.equips);
      equi.setChestplateDropChance(0.0F);
      equi.setLeggingsDropChance(0.0F);
      equi.setBootsDropChance(0.0F);
      equi.setHelmetDropChance(0.0F);
      in.setMaxHealth(this.hp);
      in.setHealth(this.hp);
      return in;
   }
}
