package com.minecave.mobmine.util;

import net.minecraft.server.v1_8_R3.EntityCaveSpider;
import net.minecraft.server.v1_8_R3.EntityLiving;
import net.minecraft.server.v1_8_R3.World;

public class AggroCaveSpider extends EntityCaveSpider {

   public AggroCaveSpider(World world) {
      super(world);
   }

   public EntityLiving getGoalTarget() {
      try {
         double e = 16.0D;
         return this.world.findNearbyPlayer(this, e);
      } catch (NullPointerException var3) {
         return null;
      }
   }
}
