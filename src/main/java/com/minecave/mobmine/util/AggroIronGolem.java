package com.minecave.mobmine.util;

import net.minecraft.server.v1_8_R3.*;
import org.bukkit.craftbukkit.v1_8_R3.util.UnsafeList;

import java.lang.reflect.Field;

public class AggroIronGolem extends EntityIronGolem {

   public AggroIronGolem(World world) {
      super(world);
      this.resetPathfinders();
      this.goalSelector.a(1, new PathfinderGoalMeleeAttack(this, 1.0D, true));
      this.goalSelector.a(2, new PathfinderGoalMoveTowardsTarget(this, 0.9D, 32.0F));
      this.goalSelector.a(4, new PathfinderGoalMoveTowardsRestriction(this, 1.0D));
      this.goalSelector.a(6, new PathfinderGoalRandomStroll(this, 0.6D));
      this.goalSelector.a(7, new PathfinderGoalLookAtPlayer(this, EntityHuman.class, 6.0F));
      this.goalSelector.a(8, new PathfinderGoalRandomLookaround(this));
      this.targetSelector.a(2, new PathfinderGoalHurtByTarget(this, false));
      this.targetSelector.a(3, new PathfinderGoalNearestAttackableTarget(this, EntityHuman.class, false, true));
   }

   private void resetPathfinders() {
      try {
         Field e = PathfinderGoalSelector.class.getDeclaredField("b");
         e.setAccessible(true);
         e.set(this.goalSelector, new UnsafeList());
         e.set(this.targetSelector, new UnsafeList());
      } catch (IllegalAccessException | NoSuchFieldException var2) {
         var2.printStackTrace();
      }

   }

   public void o(Entity e) {
      super.o(e);
      if(this.getGoalTarget() != null) {
         this.setGoalTarget(this.world.findNearbyPlayer(this, 16.0D));
      }

   }
}
