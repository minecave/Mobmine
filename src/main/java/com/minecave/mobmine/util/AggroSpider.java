package com.minecave.mobmine.util;

import net.minecraft.server.v1_8_R3.EntityHuman;
import net.minecraft.server.v1_8_R3.EntityLiving;
import net.minecraft.server.v1_8_R3.EntitySpider;
import net.minecraft.server.v1_8_R3.PathfinderGoalHurtByTarget;
import net.minecraft.server.v1_8_R3.PathfinderGoalMeleeAttack;
import net.minecraft.server.v1_8_R3.PathfinderGoalNearestAttackableTarget;
import net.minecraft.server.v1_8_R3.PathfinderGoalRandomStroll;
import net.minecraft.server.v1_8_R3.World;

import org.bukkit.entity.Player;

import com.minecave.mobmine.MinecaveMobMines;

public class AggroSpider extends EntitySpider {

   public AggroSpider(World world) {
      super(world);
      MinecaveMobMines.getPlugin().debug("AggroSpider made");
      this.targetSelector.a(1, new PathfinderGoalHurtByTarget(this, false));
      this.targetSelector.a(2, new PathfinderGoalNearestAttackableTarget(this, EntityHuman.class, true));
      this.goalSelector.a(4, new PathfinderGoalMeleeAttack(this, EntityHuman.class, 16.0D, false));
      this.goalSelector.a(7, new PathfinderGoalRandomStroll(this, 16.0D));
   }

   public void setTarget(Player player) {
      this.setTarget(player);
   }

   @Override
   public EntityLiving getGoalTarget() {
      try {
         double e = 16.0D;
         return this.world.findNearbyPlayer(this, e);
      } catch (NullPointerException var3) {
         return null;
      }
   }
}
