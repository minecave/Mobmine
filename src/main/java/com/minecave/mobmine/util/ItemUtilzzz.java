package com.minecave.mobmine.util;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

public class ItemUtilzzz {

   public static String itemToString(ItemStack in) {
      String ret = in.getType().toString() + "!" + in.getDurability() + "!" + in.getAmount() + "!";

      Entry i;
      for(Iterator i$ = in.getEnchantments().entrySet().iterator(); i$.hasNext(); ret = ret + ((Enchantment)i.getKey()).getName() + "!" + i.getValue() + "!") {
         i = (Entry)i$.next();
      }

      ret = ret.substring(0, ret.length() - 1);
      return ret;
   }

   public static List itemFromString(String ac) {
      ArrayList r = new ArrayList();
      String[] c = ac.split("\\$\\$");
      int len$ = c.length;

       for (String in : c) {
           try {
               String[] e = in.split("!");
               ItemStack ret = new ItemStack(Material.matchMaterial(e[0]), Integer.parseInt(e[2]), Short.parseShort(e[1]));
               if (ret.getType() != Material.AIR) {
                   ItemMeta met = ret.getItemMeta();
                   if (e.length >= 4) {
                       met.setDisplayName(ChatColor.translateAlternateColorCodes('&', e[3]));
                   }
                   ArrayList lore = new ArrayList();
                   if (e.length >= 5) {
                       String[] i = e[4].split("\\{n}");
                       int ench = i.length;
                       for (String i1 : i) {
                           lore.add(ChatColor.translateAlternateColorCodes('&', i1));
                       }
                       met.setLore(lore);
                   }
                   if (e.length >= 6) {
                       for (int count = 5; count < e.length; ++count) {
                           String[] enchants = e[count].split("\\|");
                           ret.addUnsafeEnchantment(Enchantment.getById(Integer.parseInt(enchants[0])), Integer.parseInt(enchants[1]));
                       }
                   }
                   ret.setItemMeta(met);
                   r.add(ret);
               }
           } catch (NullPointerException var15) {
               var15.printStackTrace();
           }
       }

      if(r.size() == 0) {
         r.add(new ItemStack(Material.AIR));
      }

      return r;
   }
}
